import numpy as np
import pandas as pd
import tkinter as tk
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
#UI
data = pd.read_csv("statsteam.csv")
dfcorr = pd.DataFrame(data)
df1 = pd.DataFrame(data, columns=['Team', 'PTS'])

#Parameter window
root = tk.Tk()
root.title("NBApp")
width  = root.winfo_screenwidth()
height = root.winfo_screenheight()
root.geometry(f'{width}x{height}')
#root.resizable(False, False)
root.iconbitmap('basketball.ico')

df1 = df1[['Team', 'PTS']].groupby('Team').sum()
fig = plt.Figure(figsize=(6, 4), dpi=100)

canvas = FigureCanvasTkAgg(fig, master=root)  # A tk.DrawingArea.
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
toolbar = NavigationToolbar2Tk(canvas, root)

OPTIONSGRAPH = ["Player Stats", "Team Stats"]
variable2 = tk.StringVar(root)
variable2.set(OPTIONSGRAPH[1]) # default value

def changedataset():
    global df1
    global OPTIONS
    global data
    global dfcorr
    if (variable2.get()== OPTIONSGRAPH[0]):
        data = pd.read_csv("statsplayer.csv")
        dfcorr = pd.DataFrame(data)
        df1 = pd.DataFrame(data, columns=['Player', variable.get()])
        df1 = df1[['Player', 'PTS']].groupby('Player').sum()
        OPTIONS = data.columns  # etc
        variable = tk.StringVar(root)
        variable.set(OPTIONS[2])  # default value
    else:
        data = pd.read_csv("statsteam.csv")
        dfcorr = pd.DataFrame(data)
        df1 = pd.DataFrame(data, columns=['Team', variable.get()])
        df1 = df1[['Team', 'PTS']].groupby('Team').sum()
        OPTIONS = data.columns  # etc
        variable = tk.StringVar(root)
        variable.set(OPTIONS[2])  # default value


CHANGEGRAPH = ["Bar", "Pie", "Hist", "Plot", "Area"]
var = tk.StringVar(root)
var.set(CHANGEGRAPH[0]) # default value

def draw_chart():
    fig.clear()
    plt.rcParams.update({'figure.autolayout': True})
    if (variable2.get() == OPTIONSGRAPH[0]):
        df1 = pd.DataFrame(data, columns=['Player', variable.get()])
        df1 = df1[['Player', ""+variable.get()]].groupby('Player').sum()
    else:
        df1 = pd.DataFrame(data, columns=['Team', variable.get()])
        df1 = df1[['Team', ""+variable.get()]].groupby('Team').sum()

    ax1 = fig.add_subplot(111)
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    print(len(df1.index))



    if (var.get() == CHANGEGRAPH[0]):
        df1.plot(kind='bar', legend=True, ax=ax1, title ='Graphe à bar  ' + variable.get(),figsize=(18.5,9.5), fontsize=6)

        for i in range(0, len(df1.index)):
            ax1.get_children()[i].set_color(colors[i % len(colors)])
        fig.autofmt_xdate(rotation=45)


    elif (var.get() == CHANGEGRAPH[1]):
        df1.plot(kind='pie', legend=False, ax=ax1, subplots=True, autopct='%1.0f%%', title="Diagramme circulaire  " + variable.get(), figsize=(18.5,9.5),fontsize = 8)
        for i in range(0, len(df1.index)):
            ax1.get_children()[i].set_color(colors[i % len(colors)])



    elif(var.get() == CHANGEGRAPH[2]):
        df1.plot(kind='hist', legend=True, ax=ax1 , title="Histogramme " + variable.get(),figsize=(18.5,9.5), fontsize=8, color='orange')



    elif (var.get() == CHANGEGRAPH[3]):
        df1.plot(legend=True, ax=ax1,subplots=True, title="Tracé " + variable.get(),figsize=(18.5,9.5), fontsize=8,color='orange')
        fig.autofmt_xdate(rotation=45)



    elif (var.get() == CHANGEGRAPH[4]):
        df1.plot(kind='area', legend=True, ax=ax1, title ="Tracé de zone " +variable.get(),figsize=(18.5,9.5), fontsize=8,color='orange')
        fig.autofmt_xdate(rotation=45)



    canvas.draw_idle()

    tk.OptionMenu(root, var, *CHANGEGRAPH).place(x=width/4, y=height-108)
    
    for i in range (0,len(df1.index)):
        ax1.get_children()[i].set_color(colors[i%len(colors)])
    canvas.draw_idle()

def show_corr():
    global dfcorr
    fig.clear()
    plt.rcParams.update({'figure.autolayout': True})
    sns.heatmap(dfcorr.corr(), xticklabels=dfcorr.corr().columns, yticklabels=dfcorr.corr().columns, cmap='RdYlGn', center=0,
                annot=True)
    plt.title('Correlogram', fontsize=22)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.get_current_fig_manager().window.state('zoomed')
    plt.show()

def show_comp():
    page=tk.Toplevel(root)
    page.title("NBApp")
    width = page.winfo_screenwidth()
    height = page.winfo_screenheight()
    page.geometry(f'{width}x{height}')
    page.iconbitmap('basketball.ico')

    OPTIONSGRAPH = data.columns #mettre liste joueur

    varleft = tk.StringVar(page)
    varleft.set(OPTIONSGRAPH[0])  # default value
    varright = tk.StringVar(page)
    varright.set(OPTIONSGRAPH[0])  # default value

    tk.OptionMenu(page,varleft,*OPTIONSGRAPH).place(x=width/2-100,y=height-108)
    tk.OptionMenu(page, varright, *OPTIONSGRAPH).place(x=width / 2 + 100, y=height - 108)



tk.Button(root,text="Draw",command=draw_chart).place(x=width/4, y=height-108)



tk.Button(root,text="Draw",command=draw_chart).place(x=width/4, y=height-108)

OPTIONS = data.columns #etc

variable = tk.StringVar(root)
variable.set(OPTIONS[2]) # default value


tk.OptionMenu(root, variable, *OPTIONS).place(x=width/4-100, y=height-108)
tk.OptionMenu(root, variable2, *OPTIONSGRAPH).place(x=width/2-100, y=height-108)


tk.Button(root,text="Change Dataset",command=changedataset).place(x=width/3+100, y=height-106)
tk.Button(root,text="Change Graph",command=draw_chart).place(x=width/5+200, y=height-106)


tk.Button(root,text="Change Dataset",command=changedataset).place(x=width/2, y=height-106)
tk.Button(root,text="Show correlation",command=show_corr).place(x=width/2+100, y=height-106)
tk.Button(root,text="face à face", command=show_comp).place(x=width/2+200, y=height-106)
root.mainloop()