import lxml
import pandas as pd
import tkinter as tk
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup
from selenium import webdriver
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

# Create a session and load the page
driver = webdriver.Chrome()
driver.get('https://global.nba.com/statistics')
driver.implicitly_wait(5)

# Parse HTML code and grab tables with Beautiful Soup
soup = BeautifulSoup(driver.page_source, 'lxml')

tables = soup.find_all('table')

# Read tables with Pandas read_html()
dfs = pd.read_html(str(tables))
#Save In a CSV File
dfs[0].to_csv("statsplayer.csv", index=False)
print(f'Total tables: {len(dfs)}')
print(dfs[0])
driver.close()
