import lxml
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver

# Create a session and load the page
driver = webdriver.Chrome()
driver.get('https://global.nba.com/statistics/teamstats/')
driver.implicitly_wait(5)

# Parse HTML code and grab tables with Beautiful Soup
soup = BeautifulSoup(driver.page_source, 'lxml')

tables = soup.find_all('table')
dfs = pd.read_html(str(tables))
#Save In a CSV File
dfs[0].to_csv("statsteam.csv", index=False)
print(dfs[0])
driver.close()